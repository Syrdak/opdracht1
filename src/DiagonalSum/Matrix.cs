using System;
using System.Collections.Generic;
using System.Text;

namespace DiagonalSum
{
    public class Matrix
    {
        public List<int[]> Rows;
        private int dimension;

        public string Dimension
        {
            get { return name; }
            set { name = value; }
        }

        public Matrix() 
        {
            Rows = new List<int[]>();
        }

        public void SetDimension(int dimension) 
        {   
            Dimension = dimension;
        }

        public void AddRow(int[] row) 
        {
            bool lengthRowIsCorrect = row.Length == Dimension;
            bool maximumNumberRowsReached = Rows.Count == Dimension;

            if (!lengthRowIsCorrect) 
            {
                Console.WriteLine($"Row must have {Dimension} elements");
            } 
            else if(maximumNumberRowsReached) 
            {
                Console.WriteLine("Maximum number of rows has been reached");
            } 
            else if(!IsRowValuesInRange(row)) 
            {
                Console.WriteLine($"Row must have numbers between -100 and 100");
            } 
            else 
            {
                Rows.Add(row);
            } 
        }

        public bool IsRowValuesInRange(int[] row) 
        {   
            foreach (int number in row) 
            {
                if (!IsNUmberInRange(number)) 
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsNUmberInRange(int number, int range = 100)
        {
            return Math.Abs(number) < range;
        }

        public string MatrixToString(string delimiter = "\t") {
            var StrBuilder = new StringBuilder();
            
            for (var rowindex = 0; rowindex < Dimension; rowindex++) 
            {
                for (var colindex = 0; colindex < Dimension; colindex++) 
                {
                    StrBuilder.Append(Rows[rowindex][colindex]).Append(delimiter);
                }
                
                StrBuilder.AppendLine();
            } 

            return StrBuilder.ToString() + "\n \n";
        }

        public double SubtractSumDiagonales() 
        {
            double result = 0.0;
            double sumDiagonal1 = 0.0;
            double sumDiagonal2 = 0.0;
            int colindex = Dimension-1;

            for (var rowindex = 0; rowindex < Dimension; rowindex++) 
            {
                sumDiagonal1 += Rows[rowindex][rowindex];
                sumDiagonal2 += Rows[rowindex][colindex];
                colindex -= 1;
            }

            result = Math.Abs(sumDiagonal1 - sumDiagonal2);

            return result;
        } 
    }
}