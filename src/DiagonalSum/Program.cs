﻿using System;
using System.Collections.Generic;

namespace DiagonalSum
{
    class Program
    {
        static void Main() {
            var matrix = new Matrix();

            while(matrix.Dimension <= 0) 
            {
                var dimension = GetDimensionFromUser();
                matrix.SetDimension(dimension);
            };

            while(matrix.Rows.Count < matrix.Dimension) 
            {
                var row = GetRowFromUser(matrix);
                matrix.AddRow(row);
            };

            Console.WriteLine("\n \n Your matrix:");
            Console.WriteLine(matrix.MatrixToString());
            var result = matrix.SubtractSumDiagonales();
            Console.WriteLine($"Absolute subtraction of diagonales equals to {result:N3}");
        }

        private static int GetDimensionFromUser() 
        {
            Console.WriteLine("Enter a positive integer for the dimensions of the matrix");
                var input = Console.ReadLine();
                var isparsesuccessful = int.TryParse(input, out var result);

                if(isparsesuccessful)
                {
                    return int.Parse(input);
                }
                else
                {
                    throw new ArgumentException("Dimension must be a positive integer");
                }
        }

        private static int[] GetRowFromUser(Matrix matrix) 
        {
            Console.WriteLine($"Enter new row");
            var input = Console.ReadLine();
            string[] numbers = input.Split();
            var row = new int[matrix.Dimension];
            try
            {
                for (int columnIndex = 0; columnIndex < matrix.Dimension; columnIndex++) 
                {
                    row[columnIndex] = int.Parse(numbers[columnIndex]);
                }
            }
            catch (System.Exception)
            {
                throw new ArgumentException($"Rows consist of integers and must have {matrix.Dimension} elements.");
            }
            return row;
        }
    }
}
