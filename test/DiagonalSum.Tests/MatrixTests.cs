using System;
using Xunit;
using DiagonalSum;

namespace DiagonalSum.Tests
{
    public class MatrixTest
    {
        [Fact]
        public void SumDiagonal()
        {
            var matrix = new Matrix();

            matrix.AddDimension(3);

            var row1 = new double[] {1.0,2, 3};
            var row2 = new double[] {4,5,6};
            var row3 = new double[] {80,8,9};

            matrix.AddRow(row1);
            matrix.AddRow(row2);
            matrix.AddRow(row3);

            var result = matrix.SubtractSumDiagonales();

            Assert.Equal(73, result);
        }
    }
}
